FROM registry.gitlab.inria.fr/inria-ci/docker/ubuntu:22.04

RUN apt update && DEBIAN_FRONTEND=noninteractive apt install -y cmake git g++ libfftw3-dev libfftw3-mpi-dev libopenmpi-dev libhdf5-openmpi-dev hdf5-tools python3-h5py sphinx-common python3-sphinx-rtd-theme 

