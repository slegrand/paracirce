ParaCirce
========

**ParaCirce** a is C++17/MPI parallel software for the generation of log-normal gaussian correlated fields.


+ C++17 compatible compiler
+ MPI >= 1.10
+ CMake >= 3.12
+ FFTW3-MPI >= 3.3.3

Build
-----

+ Submodules

Clone the project with the option ``--recurse-submodules`` to get submodules dependencies.
``` sh
git clone --recurse-submodules https://gitlab.inria.fr/paracirce/paracirce.git
```

For single configuration generator, such as Unix Makefiles, the default configuration is Release.
You can specify Debug configuration giving this command-line option to CMake: -DCMAKE_BUILD_TYPE=Debug.

``` sh
mkdir ../build
cd ../build
cmake ../paracirce [-DCMAKE_BUILD_TYPE=Debug] [-DPARACIRCE_BUILD_TESTS=ON] [-DCMAKE_INSTALL_PREFIX=<install_path>]
make
```

Tests
-----

If you set the ``PARACIRCE_BUILD_TESTS`` variable to ``ON``:

``` sh
make test
```

Installation
------------

To install **Paracirce** in the install path, execute the **install** target, e.g. on Linux:

``` sh
make install
```

Use
---

Add these lines to you CMakeLists.txt:

``` cmake
find_package(Paracirce REQUIRED)
target_link_libraries(<your_target>
    PRIVATE Paracirce::Paracirce)
```

If the **Paracirce** installation path is not standard, set Paracirce_ROOT to the installation
path when invoking cmake:

``` bash
cmake -DParacirce_ROOT=/paracirce/install/path ...
```

Refer to the [documentation](https://paracirce.gitlabpages.inria.fr/paracirce/) to learn how to use 
the **Paracirce** library.

