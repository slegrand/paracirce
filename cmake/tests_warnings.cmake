## Warning flags
#
# no-unused-parameter is set because of the fftw deallocator member takes 2
# parameters but only one is used.
# 
target_compile_options(${TEST_NAME} PRIVATE
  $<$<CXX_COMPILER_ID:MSVC>:/W4 /WX>
  $<$<NOT:$<CXX_COMPILER_ID:MSVC>>:-Wall -Wextra -Wpedantic -Wno-unused-parameter -Werror>
)
