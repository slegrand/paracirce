.. ParaCirce user documentation master file, created by
   sphinx-quickstart on Thu Dec  2 17:00:39 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to ParaCirce user documentation!
========================================================

**Parallel Circulant Embedding**

ParaCirce stands for Parallel Circulant Embedding. It is a library aimed at generating
2D and 3D stationary Gaussian Random Fields (GRF). The algorithm used is based
on the Circulant Embedding method, written in C++17 and parallelized with MPI.

It features:

- Several correlation functions
- Anisotropic correlation
- Distributed memory parallelization for optimal performances on large clusters
- Reproductibility, thanks to the pseudo-random numbers generator RngStream.
- Complete Independance of the Monte-Carlo runs.
- Acceleration of the padding computation.


.. toctree::
   :maxdepth: 2
   :caption: Contents:

   installation
   usage


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
