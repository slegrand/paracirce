ParaCirce Installation
======================

Dependencies
------------

+ C++17 compatible compiler
+ MPI >= 1.10
+ CMake >= 3.12
+ FFTW3-MPI >= 3.3.3

Build
-----

Clone the project with the option ``--recurse-submodules`` to get submodules dependencies.

.. code-block:: bash

   git clone --recurse-submodules https://gitlab.inria.fr:slegrand/paracirce.git

For single configuration generator, such as Unix Makefiles, the default configuration is Release.
You can specify Debug configuration giving this command-line option to CMake: ``-DCMAKE_BUILD_TYPE=Debug.``
Since **Paracirce** is a header only library, ``CMAKE_BUILD_TYPE`` will only
affect the embedded dependency **RngStreams**.

.. code-block:: bash

   mkdir ../build
   cd ../build
   cmake ../paracirce [-DCMAKE_BUILD_TYPE=Debug] [-DPARACIRCE_BUILD_TESTS=ON] [-DCMAKE_INSTALL_PREFIX=<install_path>] [...]
   make


ParaCirce generation options
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The following options can be given to CMake:

+ PARACIRCE_BUILD_TESTS[=OFF] Build **ParaCirce** tests
+ SIMPLE_PRECISION[=OFF] Assert float support of the fftw library
+ EXTEND_PRECISION[=OFF] Allows use of 80bit extended precision (long double)
+ PARACIRCE_USE_OMP[=ON] Allows use of OpenMP threads

Tests
-----

If you set the ``PARACIRCE_BUILD_TESTS`` variable to ``ON``:

.. code-block:: bash

   make test

Installation
------------

To install **Paracirce** in the install path, execute the **install** target, e.g. on Linux:

.. code-block:: bash

   make install

Link against ParaCirce with CMake
---------------------------------

You can either use **ParaCirce** library via the `find_package()` function from
CMake or as a submodule of your project.

find_package()
^^^^^^^^^^^^^^

Add these lines to your CMakeLists.txt:

.. code-block:: cmake

   find_package(Paracirce REQUIRED)
   target_link_libraries(<your_target> PRIVATE Paracirce::Paracirce)

If the **Paracirce** installation path is not standard, set `Paracirce_ROOT` to the installation
path when invoking cmake:

.. code-block:: Bash

   cmake -DParacirce_ROOT=/paracirce/install/path ...

submodule
^^^^^^^^^

Add the `ParaCirce repository <https://gitlab.inria.fr/slegrand/paracirce>`_
as a submodule of your project and add these lines to your CMakeLists.txt

.. code-block:: cmake

   add_subdirectory(<path/to/paracirce/>)
   target_link_libraries(<your_target> PRIVATE Paracirce::Paracirce)


Annexe: Windows
---------------

FFTW compilation
^^^^^^^^^^^^^^^^

Dependencies:
+ `SDK installers for Microsoft MPI <https://www.microsoft.com/en-us/download/details.aspx?id=100593>`_ (download and run both msmpisdk.msi and msmpisetup.exe)

1. Download `sources <http://www.fftw.org/download.html>`_
2. Replace CMakeLists.txt with modified one (fftw3-mpi target): `CMakeLists.txt <https://gitlab.inria.fr/slegrand/paracirce/-/wikis/uploads/3def019030e6563e87ca184bff9eb457/CMakeLists>`_
3. CMake:

 - Configure with Visual Studio 16 2019
 - Generate VS project Win64 with CMake with the following options:
    * ENABLE_MPI=ON
    * CMAKE_INSTALL_PREFIX=/install/path
    * BUILD_SHARED_LIBS=OFF

  - Configure again
  - Generate

4. Visual Studio 16 2019

  - Compile in Release or Debug
  - Generate Install

ParaCirce compilation
^^^^^^^^^^^^^^^^^^^^^

We manage to compile **ParaCirce** with the following dependencies versions:

+ CMake 3.12.2
+ Visual Studio 16 2019
+ FFTW 3.3.8 (with fftw_mpi)

Before running ParaCirce, you will have to create and/or modify the following environment variables (Control Panel -> System -> Advanced System Settings -> Environment Variables) :

============= =====
Variable Name Value
============= =====
 FFTW_ROOT    C:\\Path\\To\\fftw-3.3.8-mpi\\install
 MSMPI_BIN    C:\\Program Files\\Microsoft MPI\\Bin
 MSMPI_INC    C:\\Program Files (x86)\\Microsoft SDKs\\MPI\\Include
 MSMPI_LIB32  C:\\Program Files (x86)\\Microsoft SDKs\\MPI\\Lib\\x86
 MSMPI_LIB64  C:\\Program Files (x86)\\Microsoft SDKs\\MPI\\Lib\\x64
============= =====

.. code-block:: bash

   mkdir ../build && cd ../build
   cmake ../paracirce -G "Visual Studio 16 2019" [...]

Open and generate the project under Visual Studio 16 2019.
