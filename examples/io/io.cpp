/**
 * @file io.cpp
 * @brief Demonstrate how to write generated GRFs into txt and (optionnaly)
 * hdf5 formats.
 *
 * @usage mpirun -n <nb_proc> io
 *
 * @author Geraldine PICHOT and Simon LEGRAND
 * @version 0.1
 */
#include "paracirce.hpp"
#include <cmath>
#include <cstdlib>


int main(int argc, char *argv[])
{
    using namespace paracirce;

    const mpi::Environment env(argc, argv, MPI_THREAD_FUNNELED);

    mpi::Communicator paracirce_comm(MPI_COMM_WORLD);

    /** 2D field generation */
    const auto d_2D   = Domain2D<double>{{{0., 9., 32}, {0., 4., 17}}};
    const auto f_2D = Gaussian2D<double>({0.1, 0.1});
  
    auto generator_2D = GRFgenerator(MPI_COMM_WORLD, d_2D, f_2D);
    auto field_2D = generator_2D.generate();

    /** 3D field generation */
    const auto d_3D =
        Domain3D<double>{{{0., 9., 32}, {0., 4., 17}, {0., 5., 25}}};
    const auto f_3D   = Exponential3D<double>({0.1, 0.1, 0.1});

    auto generator_3D = GRFgenerator(MPI_COMM_WORLD, d_3D, f_3D);
    auto field_3D = generator_3D.generate();
  
#ifdef PARACIRCE_HAS_HDF5
    write_hdf5(field_2D, "field_2D.h5", "field", paracirce_comm);
    write_hdf5(field_3D, "field_3D.h5", "field", paracirce_comm);
#endif
    write_field_txt("field_2D.txt", field_2D.data(), paracirce_comm, 15);
    write_field_txt("field_3D.txt", field_3D.data(), paracirce_comm, 15);
    return 0;
}
