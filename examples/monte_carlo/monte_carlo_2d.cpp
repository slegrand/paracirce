/**
 * Example of use and test of the ParaCirce library. Generates nMC 2D fields and
 * writes them in separated files.
 *
 * @usage mpirun -n <nb_proc> monte_carlo_2d <nMC> <output_dir>
 * */

#include <filesystem>
#include <mpi.h>
#include <string>

#include "paracirce.hpp"

using namespace paracirce;

int main(int argc, char *argv[])
{
    const mpi::Environment env(argc, argv, MPI_THREAD_FUNNELED);

    const mpi::Communicator paracirce_comm(MPI_COMM_WORLD);

    /** Parse output directory from command line */
    if (argc != 3)
    {
        print_rank_n(paracirce_comm, 0,
                     "Error: Missing arguments: <nMC> <output_dir>");
        return (-1);
    }
    int nMC         = std::stoi(argv[1]);
    auto output_dir = std::filesystem::path(std::string(argv[2]));
    if (!std::filesystem::is_directory(output_dir))
    {
        print_rank_n(paracirce_comm, 0,
                     "Error: Output directory doesn't exists");
        return (-1);
    }

    auto BEGIN = MPI_Wtime();
    DiscretDomain<double, 2> dom{{{0., 1023, 1024}, {0., 1023, 1024}}};

    /** Isotropic Gaussian correlation function */
    auto f_correl = paracirce::Gaussian<double, 2>({1., 1.});

    /** Generator construction */
    auto generator = paracirce::GRFgenerator(paracirce_comm, dom, f_correl);

    /** Planners effort */
    generator.planners_effort("FFTW_MEASURE");

    /** Data and plans allocation */
    generator.allocate();

    if (paracirce_comm.rank() == 0)
        std::cout << "Plans creation: ~" << MPI_Wtime() - BEGIN << "s"
                  << std::endl;

    for (int mc = 0; mc < nMC; mc++)
    {
        /** Field generation */
        auto res = generator.generate();

        /** Ensure each file is fully independant */
        generator.set_rng_state(mc + 1);

        /** Write the field in file */
        write_field_txt(
            absolute(output_dir).append("field_2D_" + std::to_string(mc)),
            res.data(), paracirce_comm);
    }

    if (paracirce_comm.rank() == 0)
        std::cout << "Generate " << nMC << " fields in: " << MPI_Wtime() - BEGIN
                  << "s" << std::endl;
    return 0;
}
