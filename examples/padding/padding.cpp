#include "paracirce.hpp"
#include <vector>

using namespace paracirce;

int main(int argc, char *argv[])
{
    /** Handles MPI initialization and finalization */
    const mpi::Environment env(argc, argv, MPI_THREAD_FUNNELED);

    /** Isolate ParaCirce communications */
    mpi::Communicator paracirce_comm(MPI_COMM_WORLD);

    double BEGIN = MPI_Wtime();

    /** Cubic Domain with 64 on samples for each dimension */
    Domain2D<double> dom{{{0., 1., 64}, {0., 1., 64}}};

    /** Isotrope Matern correlation function */
    Matern2D<double> m{{0.25, 0.25}, 5};

    /** Padding estimation */
    auto dom_pad_est = padding_estimation(dom, m);

    /** Generator */
    GRFgenerator gen{paracirce_comm, dom, m};
    gen.padded_dom(dom_pad_est);

    /** Generation */
    std::shared_ptr<GRF<double, 2, Matern2D<double>>> res;
    while (res->data().empty())
    {
        paracirce_comm.barrier();
        try
        {
            res = std::make_shared<GRF<double, 2, Matern2D<double>>>(
                gen.generate());
        }
        // Don't catch all exceptions!
        catch (NegativeEigenvalue &e)
        {
            print_rank_n(paracirce_comm, 0, e.what());
            /** Increase padding while needed */
            dom_pad_est = resize(dom_pad_est, {{{0, 1}, {0, 1}}});
            std::stringstream ss;
            ss << dom_pad_est;
            print_rank_n(paracirce_comm, 0,
                         "Increasing padded domain size to " + ss.str());
            gen.padded_dom(dom_pad_est);
        }
    }
    double END = measure_elapsed_time_since_ref(paracirce_comm, BEGIN);
    print_rank_n(paracirce_comm, 0,
                 "Elapsed time: " + std::to_string(END) + "s");

    return 0;
}
