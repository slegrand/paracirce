/**
 * This file is part of the Paracirce software.
 *
 * For academic usage, you can use this software under the GNU Affero General
 * Public version 3 as published by the Free Software Foundation. Read the
 * LICENSE file in the root directory of this source tree for more information.
 *
 * For commercial usage, please contact the project members to purchase this
 * software under proprietary license,
 *
 * Copyright (C) 2021, Inria
 *
 * @file paracirce.hpp
 *
 * @brief Implementation of the Circulant Embedding algorithm
 *
 * @author Geraldine PICHOT and Simon LEGRAND
 */

#ifndef GRF_H_
#define GRF_H_

#include "paracirce/grf.hpp"
#include "paracirce/io_utils.hpp"
#include "paracirce/padding.hpp"

namespace paracirce
{

template <typename T> using Domain2D            = DiscretDomain<T, 2>;

template <typename T> using Domain3D            = DiscretDomain<T, 3>;

template <typename T> using Exponential2D       = Exponential<T, 2>;

template <typename T> using Exponential3D       = Exponential<T, 3>;

template <typename T> using Exponential_norm12D = Exponential_norm1<T, 2>;

template <typename T> using Exponential_norm13D = Exponential_norm1<T, 3>;

template <typename T> using Gaussian2D          = Gaussian<T, 2>;

template <typename T> using Gaussian3D          = Gaussian<T, 3>;

template <typename T> using Matern2D            = Matern<T, 2>;

template <typename T> using Matern3D            = Matern<T, 3>;

template <typename T, typename Function>
using GRFgenerator2D = GRFgenerator<T, 2, Function>;

template <typename T, typename Function>
using GRFgenerator3D = GRFgenerator<T, 3, Function>;

} // namespace paracirce
#endif /* GRF_H_ */
