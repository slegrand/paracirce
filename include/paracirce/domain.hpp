/**
 * This file is part of the Paracirce software.
 *
 * For academic usage, you can use this software under the GNU Affero General
 * Public version 3 as published by the Free Software Foundation. Read the
 * LICENSE file in the root directory of this source tree for more information.
 *
 * For commercial usage, please contact the project members to purchase this
 * software under proprietary license,
 *
 * Copyright (C) 2021, Inria
 *
 * @file domain.hpp
 *
 * @brief Representation of a generic cartesian domain, defined
 * by its bounds and the number of samples along each dimension.
 * It doesn't store the samples, but acts as a generator.
 *
 * @author Geraldine PICHOT and Simon LEGRAND
 */

#ifndef __DOMAIN_H_
#define __DOMAIN_H_

#include <array>
#include <cassert>
#include <cstddef>
#include <iostream>
#include <limits>
#include <numeric>
#include <vector>

namespace paracirce
{

/**
 * @brief Regularly sampled segment representation
 * Defined by its left and rights bounds and
 * the number of samples. It is then a virtual container.
 *
 * Domain with n_ = 0 or 1 can't be resized since the step
 * is not defined. In these cases, l_bound = r_bound_.
 * */
template <typename T> class Range
{
    /** [l_bound,r_bound] */
    T l_bound_{0.0};
    T r_bound_{0.0};

    /** Step between samples */
    T step_{0.0};

    /** Number of samples */
    size_t n_{0};

  public:
    Range() = default;
    Range(const T l_b, const T r_b, const size_t n)
        : l_bound_(l_b), r_bound_(r_b), n_(n)
    {
      assert((long long)n_ >= 0);
        if (n_ > 1)
        {
            assert(l_bound_ != r_bound_);
            if (l_bound_ > r_bound_)
                std::swap(l_bound_, r_bound_);
            step_ = (r_bound_ - l_bound_) / (n_ - 1);
        }
        else
            assert(l_bound_ == r_bound_);
    };

    class iterator;
    iterator begin() { return iterator(l_bound_, this); }
    iterator end() { return iterator(0, nullptr); }

    /** Getters */
    T l_bound() const { return l_bound_; }
    T r_bound() const { return r_bound_; }
    auto bounds() const { return std::pair<T, T>(l_bound_, r_bound_); };
    auto n() const { return n_; };
    T step() const { return step_; }

    /** Modifiers */

    /** Add or remove samples on the right of the Range */
    void resize_right(const long long r_offset)
    {
      assert((long long)n_ + r_offset >= 0);
        if (n_ > 1)
        {
            n_ += r_offset;
            if (n_ > 1)
                r_bound_ += r_offset * step_;
            else
            {
                n_       = 0;
                r_bound_ = l_bound_;
            }
        }
        else
        {
            std::cout << "Warning: can't resize domain with " << n_
                      << "elements, step is not defined." << '\n';
        }
    }
    /** Add or remove samples on the left of the Range */
    void resize_left(const long long l_offset)
    {
      assert((long long)n_ + l_offset >= 0);
        if (n_ > 1)
        {
            n_ += l_offset;
            if (n_ > 1)
                l_bound_ -= l_offset * step_;
            else
            {
                n_       = 0;
                l_bound_ = r_bound_;
            }
        }
        else
        {
            std::cout << "Warning: can't resize domain with " << n_
                      << "elements, step is not defined." << '\n';
        }
    }
};

/**
 * @brief Return a resized Range at constant step.
 * If range is empty,
 *
 * @param rg: orginal Range
 * @param offsets.first: number of samples to add/remove on the left
 * @param offsets.second: number of samples to add/remove on the right
 * */
template <typename T>
Range<T> resize(const Range<T> &rg,
                const std::pair<long long, long long> offsets)
{
    auto resized_range(rg);
    resized_range.resize_left(offsets.first);
    resized_range.resize_right(offsets.second);
    return resized_range;
}

template <typename T> std::ostream &operator<<(std::ostream &os, Range<T> r)
{
    os << "[" << r.l_bound() << ", " << r.r_bound() << "]" << " - " << r.n()
       << " samples" << " - step: " << r.step();
    return os;
}

template <typename T> class Range<T>::iterator
{
    T value_{};
    Range *range_;
    size_t ctr_{0};
    void check_done()
    {
        if (at_end())
        {
            range_ = nullptr;
        }
    }
    /** Helper for the postincrement */
    class postinc_return
    {
        T value_;

      public:
        postinc_return(T value) : value_(std::move(value)) {}
        T operator*() { return std::move(value_); }
    };

  public:
    using value_type        = T;
    using reference         = T;
    using iterator_category = std::input_iterator_tag;
    using pointer           = T *;
    using difference_type   = void;

    iterator(T val, Range *r) : value_(val), range_(r)
    {
        if (range_)
            check_done();
    }

    T operator*() const { return value_; }

    T *operator->() const { return &value_; }

    iterator &operator++()
    {
        value_ += range_->step();
        // range->inc(range->current);
        ctr_++;
        check_done();
        return *this;
    }
    postinc_return operator++(int)
    {
        postinc_return temp(**this);
        ++*this;
        return temp;
    }

    friend bool operator==(iterator const &lhs, iterator const &rhs)
    {
        return lhs.range_ == rhs.range_;
    }

    friend bool operator!=(iterator const &lhs, iterator const &rhs)
    {
        return !(lhs == rhs);
    }

    bool at_end() { return ctr_ >= range_->n(); }
};

/**
 * From
 * https://randomascii.wordpress.com/2012/02/25/comparing-floating-point-numbers-2012-edition/
 * */
template <typename T>
bool almost_equal(T a, T b, T max_rel_diff = std::numeric_limits<T>::epsilon())
{
    const auto diff    = std::abs(a - b);
    const auto A       = std::abs(a);
    const auto B       = std::abs(b);
    const auto largest = (B > A) ? B : A;
    if (diff <= largest * max_rel_diff)
        return true;
    return false;
};

/**
 * Should be good enough criteria for testing equality...
 * WARNING: Use with caution.
 * Default value of max_rel_limits of almost_equal is too restrictive, 1e-5 is
 * fine for the tests.
 * */
template <typename T> bool operator==(Range<T> r1, Range<T> r2)
{
    if (r1.n() == r2.n() && almost_equal(r1.l_bound(), r2.l_bound(), 1e-5) &&
        almost_equal(r1.r_bound(), r2.r_bound(), 1e-5))
        return true;
    return false;
};

/**
 * @brief Represent a discretized cartesian domain.
 *
 * It is implemented as an ensemble of regularly sampled segments.
 */
template <typename T, size_t DIM>
using DiscretDomain = std::array<Range<T>, DIM>;

/**
 * @brief Return array filled with the bounds of the domain along each dimension
 * */
template <typename T, size_t DIM>
std::array<std::pair<T, T>, DIM> bounds(const DiscretDomain<T, DIM> &dom)
{
    std::array<std::pair<T, T>, DIM> bds;
    for (size_t i = 0; i < DIM; ++i)
    {
        bds[i].first  = dom[i].l_bound();
        bds[i].second = dom[i].r_bound();
    }
    return bds;
}

/**
 * @brief Return array filled with domain number of samples along each dimension
 * */
template <typename T, size_t DIM>
std::array<size_t, DIM> nb_samples(const DiscretDomain<T, DIM> &dom)
{
    std::array<size_t, DIM> N;
    for (size_t i = 0; i < DIM; ++i)
        N[i] = dom[i].n();

    return N;
}

/**
 * @brief Return the total number of samples in a DiscretDomain
 * */
template <typename T, size_t DIM> size_t size(const DiscretDomain<T, DIM> &dom)
{
    auto n = nb_samples(dom);
    return std::accumulate(n.begin(), n.end(), size_t(1),
                           std::multiplies<size_t>());
}

/**
 * @brief Return array filled with domain steps along each dimension
 * */
template <typename T, size_t DIM>
std::array<T, DIM> steps(const DiscretDomain<T, DIM> &dom)
{
    std::array<T, DIM> h{};
    for (size_t i = 0; i < DIM; i++)
        h[i] = dom[i].step();
    return h;
}

/**
 * @brief Return array filled with domain lengths along each dimension
 * */
template <typename T, size_t DIM>
std::array<T, DIM> lengths(const DiscretDomain<T, DIM> &dom)
{
    std::array<T, DIM> lgths;
    for (size_t i = 0; i < DIM; ++i)
    {
        lgths[i] = dom[i].r_bound() - dom[i].l_bound();
    }
    return lgths;
}

/**
 * Should be good enough criteria for testing equality...
 * */
template <typename T, size_t DIM>
bool operator==(const DiscretDomain<T, DIM> &d1,
                const DiscretDomain<T, DIM> &d2)
{
    for (size_t i = 0; i < DIM; i++)
        if (!(d1[i] == d2[i]))
            return false;
    return true;
};

/**
 * @brief Return a resized domain at constant step.
 *
 * @param rg: orginal domain
 * @param offsets: array of pair of samples to remove on each dimension
 *        each pair refers to left and right side of the domain
 * */
template <typename T, size_t DIM>
DiscretDomain<T, DIM> resize(
    const DiscretDomain<T, DIM> init_dom,
    const std::array<std::pair<long long, long long>, DIM> offsets)
{
    DiscretDomain<T, DIM> res{};
    for (size_t i = 0; i < DIM; i++)
        res[i] = resize(init_dom[i], offsets[i]);
    return res;
};

/**
 * @brief Return a subdomain delimited by given indices of points in
 * the parent domain.
 * */
template <typename T, size_t DIM>
DiscretDomain<T, DIM> create_subdomain(
    DiscretDomain<T, DIM> const &parentDom,
    std::array<std::pair<size_t, size_t>, DIM> const &subDomIdx)
{
    std::array<Range<T>, DIM> new_range_set;
    for (size_t i = 0; i < DIM; ++i)
    {
        const auto [l_idx, r_idx] = subDomIdx[i];

        /** Assert given indices belong to the parent domain*/
        if (l_idx < parentDom[i].n() && r_idx < parentDom[i].n())
        {
            T step    = parentDom[i].step();
            T par_l_b = parentDom[i].l_bound();
            new_range_set[i] =
                Range<T>(par_l_b + l_idx * step, par_l_b + r_idx * step,
                         r_idx - l_idx + 1);
        }
        else
        {
            throw std::runtime_error(
                "Required subdomain isn't included in parent DiscretDomain.");
        }
    }
    return DiscretDomain<T, DIM>(new_range_set);
}

/**
 * @brief Print utility for DiscretDomain objects
 * */
template <typename T, size_t DIM>
std::ostream &operator<<(std::ostream &os, const DiscretDomain<T, DIM> dom)
{
    for (const auto &range : dom)
        os << range << std::endl;
    return os;
}

/**
 * @brief Sample an subinterval with n_local points, located in a
 * symmetrized global interval with n_global points.
 * Warning: The first point of the global domain is not symmetrized.
 * Results depends on the parity of n_global.
 *
 * @param T step:             Step between each sample
 * @param size_t n_local:     Number of samples returned
 * @param size_t lower_bound: Index of the begginning of the interval
 * to sample in the global interval.
 * @param size_t n:           Number of samples
 *
 * @returns std:vector<T> with n elements.

 * Example:
 * symmetric_sampling_1d(1., 10, 0, 10)
 * returns (0.,1.,2.,3.,4.,5.,4.,3.,2.,1.)
 *
 * symmetric_sampling_1d(1., 9, 0, 9)
 * returns (0.,1.,2.,3.,4.,4.,3.,2.,1.)
 */
template <typename T>
std::vector<T> symmetric_sampling_1d(T const step, size_t const n_local,
                                     size_t const lower_bound,
                                     size_t const n_global)
{
    std::vector<T> x(n_local);
    size_t middle = n_global / 2;
    size_t start  = lower_bound;
    size_t end    = lower_bound + n_local;

    if (end <= middle)
        for (size_t i = start; i < end; i++)
            x[i - start] = step * i;

    else if (start > middle)
        for (size_t i = start; i < end; i++)
            x[i - start] = step * (n_global - i);

    else
    {
        size_t cntr = 0;
        for (size_t i = start; i <= middle; i++)
        {
            x[cntr] = step * i;
            cntr++;
        }
        for (size_t i = middle + 1; i < end; i++)
        {
            x[cntr] = step * (n_global - i);
            cntr++;
        }
    }
    return x;
};

} // namespace paracirce
#endif // __DOMAIN_H_
