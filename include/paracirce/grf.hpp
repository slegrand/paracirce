/**
 * This file is part of the Paracirce software.
 *
 * For academic usage, you can use this software under the GNU Affero General
 * Public version 3 as published by the Free Software Foundation. Read the
 * LICENSE file in the root directory of this source tree for more information.
 *
 * For commercial usage, please contact the project members to purchase this
 * software under proprietary license,
 *
 * Copyright (C) 2021, Inria
 *
 * @file grf.hpp
 *
 * @brief Implementation of the Circulant Embedding method
 *
 * @author Geraldine PICHOT and Simon LEGRAND
 * */
#ifndef __GRF_H_
#define __GRF_H_

#include "domain.hpp"
#include "fft_api.hpp"
#include "functions.hpp"
#include "io_utils.hpp"
#include <fftw3-mpi.h>
#include <fftw3.h>
#ifdef WITH_JSON
#include "logging.hpp"
#endif
#include "mpi_utils.hpp"
#include "rng.hpp"
#include <fstream>
#include <iostream>
#include <map>

namespace paracirce
{
  
class NegativeEigenvalue : public std::runtime_error
{
  public:
    // Defining constructor of class Exception
    // that passes a string message to the runtime_error class
    NegativeEigenvalue()
        : std::runtime_error(
              "Negative eigen value detected, increase the padding.")
    {
    }
};

class UnknownPlannersEffort : public fftw::UnknownPlannerFlag
{
  public:
    // Defining constructor of class Exception
    // that passes a string message to the runtime_error class
  UnknownPlannersEffort(std::string msg) : fftw::UnknownPlannerFlag(msg) {}
};


/**
 * @brief Class representing a Gaussian Rando Field (GRF)
 * */
template <typename T, size_t DIM, typename Function> class GRF
{
    using Domain = DiscretDomain<T, DIM>;

  private:
    mpi::Communicator comm_;
    /**
     * @brief local GRF data
     * */
    std::vector<T> data_;
    Domain local_dom_;
    Domain global_dom_;
    Function correl_function_;

  public:
    GRF(const mpi::Communicator &comm, std::vector<T> data, Domain local_dom,
        Domain global_dom, Function correl_function)
        : comm_{comm}, data_{std::move(data)}, local_dom_{std::move(local_dom)},
          global_dom_{std::move(global_dom)},
          correl_function_{std::move(correl_function)}
    {
    }

    Domain const &local_dom() const { return local_dom_; };
    Domain const &global_dom() const { return global_dom_; };
    mpi::Communicator const &comm() const { return comm_; };
    std::vector<T> const &data() const { return data_; };

    // TODO: Ajouter des accesseurs/iterateurs?
};

/**
 * Main class of the ParaCirce library. Build from a Domain and a
 * correlation Function, is expose the method generate to return a GRF.
 * */
template <typename T, size_t DIM, typename Function> class GRFgenerator
{
    static_assert((DIM == 2 || DIM == 3), "Only 2D and 3D cases are handled");
    static_assert(std::is_floating_point_v<T>,
                  "The Generator only returns floating point values");

    using Domain = DiscretDomain<T, DIM>;
    using FFT    = fftw::FFTW<T, DIM>;

  private:
    mpi::Communicator comm_;

    /**
     * @brief Global cartesian domain on which the returned GRF is defined.
     * Requested by caller.
     * */
    Domain dom_;

    /**
     * @brief Cartesian padded domain. dom_global_ce_ is defined as the
     * symmetric of this domain. Padding may be necessary to ensure statistical
     * properties of the returned GRF.
     * */
    Domain padded_dom_;

    /**
     * @brief Cartesian domain on which circulant embedding algorithm is
     * performed on this domain.
     * */
    Domain ce_global_dom_;

    /**
     * @brief Local subdomain of dom_ce_global handled by a MPI process.
     * */
    Domain ce_local_dom_;

    /**
     * @brief Correlation function
     * */
    Function correlation_function_;

    /**
     * @brief Objects handling the two FFT, forward and backward
     **/
    FFT fourier_forward_;
    FFT fourier_backward_;

    /**
     * Supported planner efforts for the underlying FFTW.
     */
    const std::vector<std::string> SUPPORTED_PLANNERS_EFFORTS_{
        "FFTW_ESTIMATE", "FFTW_MEASURE", "FFTW_PATIENT", "FFTW_EXHAUSTIVE"};

  std::string planners_effort_{ "FFTW_ESTIMATE"};

    /**
     * @brief Set to true if data_ has been preallocated and fftw plans set.
     * */
    bool is_planned_ = false;

    /**
     * @brief Random number generator
     * @todo Could this be removed from the class?
     */
    Rng rng_{};

    /**
     * @brief If threshold_ < x < 0
     *        -> x = 0
     * */
    T threshold_{-1e-13};

    /**
     *  @brief Container used by the algorithm. Differ from the one returned by
     *  generate.
     *  */
    std::vector<std::complex<T>, fftw::fftw_allocator<std::complex<T>>> data_;

  public:
    GRFgenerator()                               = delete;

    GRFgenerator(GRFgenerator const &)           = delete;
    GRFgenerator operator=(const GRFgenerator &) = delete;

    GRFgenerator(const mpi::Communicator &comm, Domain d, Function f);

    // TODO: Implement move constructors
    ~GRFgenerator();

    FFT const &fourier_forward() { return fourier_forward_; };
    FFT const &fourier_backward() { return fourier_backward_; };

    Domain compute_ce_global_domain();

    /*
     * @brief Flags used by the fftw planners. Common to both forward and
     * backward transforms.
     * */
    void planners_effort(const std::string &);
    std::string const planners_effort(){return planners_effort_;};

    /*
     * @brief Preallocate data_ and fftw plans.
     * @warning data_ isn't deallocate at the end of generate(). To release
     * the memory before destruction of the object, deallocate must be
     * explicitly called.
     * */
    void allocate();

    /*
     * @brief Deallocate memory owned by data_ and free plans.
     * */
    void deallocate();
    bool is_planned() { return is_planned_; };

    Domain const &padded_dom() { return padded_dom_; }
    void padded_dom(const Domain &new_padded_dom);

    T threshold() { return threshold_; }
    void threshold(const T new_threshold) { threshold_ = new_threshold; };

    /**
     * @brief Returns a GRF
     * */
    GRF<T, DIM, Function> generate();
    GRF<T, DIM, Function> generate(size_t nx_local);

    void symmetric_correl_func_discretization(
        std::vector<std::complex<T>, fftw::fftw_allocator<std::complex<T>>>
            &data) const;
    std::vector<T> build_champ(
        const size_t ny_input, const size_t nz_input, const size_t nijk_i,
        const size_t Ny_fft, const size_t Nz_fft, const size_t local_nx,
        const size_t local_x_start,
        const std::vector<std::complex<T>,
                          fftw::fftw_allocator<std::complex<T>>> &data);

    /**
     * @brief Change the global seed, instanciated object represent different
     * streams.
     * */
    static void set_rng_global_seed(const std::array<unsigned long, 6> seed);

    /**
     * @brief Move the RNG to sub_s th substream
     * */
    void set_rng_state(const size_t sub_s);
};

template <typename T, size_t DIM, typename Function>
GRFgenerator<T, DIM, Function>::~GRFgenerator()
{
#ifndef NDEBUG
    std::cout << "-- GRFgenerator --" << std::endl;
#endif
}

/**
 * @brief Compute the Circulant Emdedding global domain from
 * the padded domain, n --> 2n-2.
 * */
template <typename T, size_t DIM, typename Function>
DiscretDomain<T, DIM> GRFgenerator<T, DIM, Function>::compute_ce_global_domain()
{
    auto offsets = std::array<std::pair<long long, long long>, DIM>{};
    auto N       = nb_samples(padded_dom_);
    for (size_t i = 0; i < DIM; i++)
        offsets[i] = {0, N[i] - 2};
    return resize(padded_dom_, offsets);
}

template <typename T, size_t DIM, typename Function>
GRFgenerator<T, DIM, Function>::GRFgenerator(const mpi::Communicator &comm,
                                             Domain d, Function f)
    : comm_(comm), dom_(std::move(d)), padded_dom_(dom_),
      ce_global_dom_(compute_ce_global_domain()),
      correlation_function_(std::move(f)),
      fourier_forward_(FFT(comm_, -1, nb_samples(ce_global_dom_))),
      fourier_backward_(FFT(comm_, 1, nb_samples(ce_global_dom_)))
{
    /*
     * Construct local domain handled by the current process
     * */
    ce_local_dom_ = ce_global_dom_;
    T x_step      = steps(ce_global_dom_)[0];
    T x_l_b       = fourier_forward_.local_x_start() * x_step;
    T x_r_b       = x_l_b + (fourier_forward_.local_nx() - 1) * x_step;
    if (fourier_forward_.local_nx() == 0)
        x_r_b = x_l_b;
    ce_local_dom_[0] = Range<T>(x_l_b, x_r_b, fourier_forward_.local_nx());

    /**
     * Initialize the random number generator.
     * Warning multiply by 2 (real and im)
     *
     * @todo Check si l'input a été transposé
     * */
    size_t nb_skip =
        (paracirce::size(ce_global_dom_) / nb_samples(ce_global_dom_)[0]) *
        fourier_forward_.local_x_start();
    rng_.set_position(2 * nb_skip);

#ifndef NDEBUG
    std::cout << "++ GRFgenerator ++" << std::endl;
#endif
}

template <typename T, size_t DIM, typename Function>
void GRFgenerator<T, DIM, Function>::planners_effort(const std::string &effort)
{
    auto elem_it = std::find(SUPPORTED_PLANNERS_EFFORTS_.begin(),
                             SUPPORTED_PLANNERS_EFFORTS_.end(), effort);
    if (elem_it != SUPPORTED_PLANNERS_EFFORTS_.end())
    {
        /* Invalidate previously allocated plans */
        if (is_planned_)
            deallocate();

        planners_effort_ = *elem_it;
    }
    else
    {
        std::ostringstream os;
        os << "Unknown planners effort. Possible values are:\n";
        for (const auto &val : SUPPORTED_PLANNERS_EFFORTS_)
            os << "- " << val << "\n";
        throw UnknownPlannersEffort(os.str());
    }
}

template <typename T, size_t DIM, typename Function>
void GRFgenerator<T, DIM, Function>::allocate()
{
    print_rank_n(comm_, 0,
                 "Paracirce: Plans creation and memory allocation...");
    data_.reserve(fourier_forward_.alloc_local());
    data_.resize(paracirce::size(ce_local_dom_));

    fourier_forward_.set_plan(data_, data_, {planners_effort_});
    fourier_backward_.set_plan(data_, data_, {planners_effort_});

    is_planned_ = true;
}

template <typename T, size_t DIM, typename Function>
void GRFgenerator<T, DIM, Function>::deallocate()
{
    print_rank_n(comm_, 0,
                 "Paracirce: Plans deletion and memory deallocation...");
    data_.clear();
    data_.shrink_to_fit();

    fourier_forward_.free_plan();
    fourier_backward_.free_plan();

    is_planned_ = false;
}

  /**
   * @todo Fix nb_skip for transposed transorm
   */
template <typename T, size_t DIM, typename Function>
void GRFgenerator<T, DIM, Function>::padded_dom(const Domain &new_padded_dom)
{
    // Assert the new domain includes the initial domain (faire un predicat
    // is_included() dans Domain.hpp?)
    for (size_t i = 0; i < DIM; ++i)
    {
        assert(almost_equal(new_padded_dom[i].step(), dom_[i].step()));
        assert(new_padded_dom[i].l_bound() == dom_[i].l_bound());
        assert(new_padded_dom[i].r_bound() >= dom_[i].r_bound());
    }
    padded_dom_    = new_padded_dom;

    // Extended domain
    ce_global_dom_ = compute_ce_global_domain();

    /* Invalidate both data_ and plans. */
    if (is_planned_)
        deallocate();

    fourier_forward_  = FFT(comm_, -1, nb_samples(ce_global_dom_));
    fourier_backward_ = FFT(comm_, 1, nb_samples(ce_global_dom_));

    /* Construct local domain handled by the current process */
    ce_local_dom_     = ce_global_dom_;
    T x_step          = steps(ce_global_dom_)[0];
    T x_l_b           = fourier_forward_.local_x_start() * x_step;
    T x_r_b           = x_l_b + (fourier_forward_.local_nx() - 1) * x_step;
    if (fourier_forward_.local_nx() == 0)
        x_r_b = x_l_b;
    ce_local_dom_[0] = Range<T>(x_l_b, x_r_b, fourier_forward_.local_nx());

    // Initialize the random number generator
    // Warning multiply by 2 (real and im)
    size_t nb_skip   = (size(ce_global_dom_) / nb_samples(ce_global_dom_)[0]) *
                     fourier_forward_.local_x_start();
    rng_.set_position(2 * nb_skip);
}

template <typename T>
void write_size_min_max_eig(const T s, const T m, const T M,
                            const std::string &filename)
{
    std::ofstream myfile;
    myfile.open(filename, std::ios::app);
    myfile << s << " " << m << " " << M << std::endl;
    myfile.close();
}

template <typename T, size_t DIM, typename Func>
void GRFgenerator<T, DIM, Func>::set_rng_global_seed(
    const std::array<unsigned long, 6> seed)
{
    Rng::set_global_seed(seed);
}

template <typename T, size_t DIM, typename Func>
void GRFgenerator<T, DIM, Func>::set_rng_state(const size_t sub_s)
{
    rng_.set_substream(sub_s);
}

template <typename T, size_t DIM, typename Func>
void GRFgenerator<T, DIM, Func>::symmetric_correl_func_discretization(
    std::vector<std::complex<T>, fftw::fftw_allocator<std::complex<T>>> &data)
    const
{
    size_t const local_nx      = fourier_forward_.local_nx();
    size_t const local_x_start = fourier_forward_.local_x_start();

    auto d                     = steps(ce_local_dom_);
    auto n_global              = nb_samples(ce_global_dom_);
    /**
     * Instead of symmetrizing the Function by testing wether a point is beyond
     * the center of symmetry, we instead symmetrize the domain.
     * */
    auto const x_sym =
        symmetric_sampling_1d(d[0], local_nx, local_x_start, n_global[0]);
    auto const y_sym = symmetric_sampling_1d(d[1], n_global[1], 0, n_global[1]);

    if constexpr (DIM == 2)
    {
        sampling_complex_2D(correlation_function_, x_sym, y_sym, data);
    }
    else
    {
        auto const z_sym =
            symmetric_sampling_1d(d[2], n_global[2], 0, n_global[2]);
        sampling_complex_3D(correlation_function_, x_sym, y_sym, z_sym, data);
    }
}

/**
 * @brief Share received by the r-th entity among n after a balanced sharing of
 * n_items.
 * Last entities share the rest of n_items / n.
 * */
size_t balanced_sharing(size_t r, size_t n, size_t n_items)
{
    size_t share;
    if (n_items < n)
    {
        if (r < n_items)
            share = 1;
        else
            share = 0;
    }
    else
    {
        share = n_items / n;
        if (r >= n - (n_items % n))
            share += 1;
    }
    return share;
}

/**
 * @brief All the shares received by n entities after a balanced sharing of
 * n_items.
 * Last entities share the rest of n_items / n.
 * */
std::vector<size_t> balanced_sharing_v(size_t n, size_t n_items)
{
    std::vector<size_t> shares(n);
    for (size_t r = 0; r < n; r++)
    {
        size_t share;
        if (n_items < n)
        {
            if (r < n_items)
                share = 1;
            else
                share = 0;
        }
        else
        {
            share = n_items / n;
            if (r >= n - (n_items % n))
                share += 1;
        }
        shares[r] = share;
    }
    return shares;
}

#ifndef PARACIRCE_HAS_OMP
/**
 *@brief  Collective operation. Set eigenvalue to 0 if it is between threshold
 *        and 0.
 *@return true if eigenvalues are all greater or equal to 0.
 *        false if not.
 * */
template <typename T>
bool positive_eigenvalues(
    const mpi::Communicator &comm,
    std::vector<std::complex<T>, fftw::fftw_allocator<std::complex<T>>> &ev,
    T threshold)
{
    bool flag = true;
    for (auto &c: ev)
    {
        if (c.real() < threshold)
        {
            flag = false;
	    break;
        }
        else if (c.real() < 0)
            c.real(0.);
    }
    flag = comm.all_reduce(int(flag), MPI_PROD);
    return flag;
}

#else
/**
 *@brief  Collective operation. Set eigenvalue to 0 if it is between threshold
 *        and 0.
 *@return true if eigenvalues are all greater or equal to 0.
 *        false if not.
 * */
template <typename T>
bool positive_eigenvalues(
    const mpi::Communicator &comm,
    std::vector<std::complex<T>, fftw::fftw_allocator<std::complex<T>>> &ev,
    T threshold)
{
    bool flag = true;
#pragma omp parallel for
    for (auto c = ev.begin(); c != ev.end(); ++c)
    {
        if (c->real() < threshold)
        {
            flag = false;
        }
        else if (c->real() < 0)
            c->real(0.);
    }
    flag = comm.all_reduce(int(flag), MPI_PROD);
    return flag;
}
#endif

#ifndef PARACIRCE_HAS_OMP
/**
 * @brief Compute 1/sqrt(scale) * sqrt(D) * theta,
 * where: D is eigen values matrix and
 *        theta = gv_re + i*gv_im
 * Cf. step 4 algo 2 before ifft
 * */
template <typename T>
void compute_phases(
    std::vector<std::complex<T>, fftw::fftw_allocator<std::complex<T>>> &data,
    Rng &rng, const size_t global_dom_size)
{
    const T inv_sqrt_scale = 1. / sqrt(global_dom_size);
    T re, im;
    std::for_each(
        data.begin(), data.end(), [&, inv_sqrt_scale](std::complex<T> &d) {
            auto gaussian_val = std_complex_normal<T>(rng);
            // Seems tob be a bug when expression is directly passed
            // to real() method.
            re = sqrt(d.real()) * gaussian_val.real() * inv_sqrt_scale;
            im = sqrt(d.real()) * gaussian_val.imag() * inv_sqrt_scale;
            d.real(re);
            d.imag(im);
        });
}
#else
template <typename T>
void compute_phases(
    std::vector<std::complex<T>, fftw::fftw_allocator<std::complex<T>>> &data,
    const Rng &rng, const size_t global_dom_size)
{
    const T inv_sqrt_scale = 1. / sqrt(global_dom_size);
    const size_t N         = data.size();
#pragma omp parallel
    {
        T re, im = 0.0;
        const size_t id  = omp_get_thread_num();
        const size_t nth = omp_get_num_threads();
        std::vector<size_t> chunks(nth);
        for (size_t i = 0; i < nth; i++)
            chunks[i] = balanced_sharing(i, nth, N);
        size_t i_start = 0;
        for (size_t i = 1; i < id + 1; i++)
            i_start += chunks[i - 1];
        const size_t i_end = i_start + chunks[id];
        /** Local copy of the generator */
        Rng rng_loc        = rng;

        /** Set proper start of rng in the stream */
        rng_loc.set_position(i_start * 2);

        std::complex<T> gaussian_val{};
        for (size_t i = i_start; i < i_end; i++)
        {
            gaussian_val = std_complex_normal<T>(rng_loc);

            re = sqrt(data[i].real()) * gaussian_val.real() * inv_sqrt_scale;
            im = sqrt(data[i].real()) * gaussian_val.imag() * inv_sqrt_scale;

            data[i].real(re);
            data[i].imag(im);
        }
    }
}
#endif

template <typename T, size_t DIM, typename Function>
GRF<T, DIM, Function> GRFgenerator<T, DIM, Function>::generate(
    const size_t nx_local)
{
    print_rank_n(comm_, 0, "Paracirce: Generating GRF...");
    bool handle_alloc = false;
    if (!is_planned_)
    {
        allocate();
        handle_alloc = true;
    }

    print_rank_n(comm_, 0, "Paracirce: Correlation function discretization...");
    symmetric_correl_func_discretization(data_);

    print_rank_n(comm_, 0, "Paracirce: FFT...");
    fourier_forward_.execute();

#if !NDEBUG && WITH_JSON
    auto min = mpi::get_min_parallel(comm_, data_);
    auto max = mpi::get_max_parallel(comm_, data_);
    Logger::Instance()("ev_min", min);
    Logger::Instance()("ev_max", max);
#endif

    // Set to 0 negative value between threshold and zero
    if (!positive_eigenvalues(comm_, data_, threshold_))
    {
        fourier_forward_.free_plan();
        fourier_backward_.free_plan();
#if !NDEBUG && WITH_JSON
        Logger::Instance().write("dump.txt");
#endif
        throw NegativeEigenvalue();
    }

    print_rank_n(comm_, 0, "Paracirce: Phases computation...");
    compute_phases(data_, rng_, paracirce::size(ce_global_dom_));

    print_rank_n(comm_, 0, "Paracirce: Inverse FFT...");
    fourier_backward_.execute();

    print_rank_n(comm_, 0, "Paracirce: Field retrieval...");
    size_t ny_requested = nb_samples(dom_)[1];
    size_t ny_ce        = nb_samples(ce_global_dom_)[1];
    size_t nz_requested = 1;
    size_t nz_ce        = 1;
    if constexpr (DIM == 3)
    {
        nz_requested = nb_samples(dom_)[2];
        nz_ce        = nb_samples(ce_global_dom_)[2];
    }
    std::vector<T> raw_field(build_champ(
        ny_requested, nz_requested, nx_local, ny_ce, nz_ce,
					 fourier_forward_.local_nx(), fourier_forward_.local_x_start(), data_));

    // Compute the local Domain of the returned GRF owned by the current process
    auto nx_locals = comm_.all_gather(nx_local);
    auto rank = comm_.rank();
    size_t idx_first_elem = std::accumulate(nx_locals.begin(),
					    nx_locals.begin()+rank,
					    std::size_t(0));

    size_t nb_elem_left = 0;
    if(rank < comm_.size() - 1)
      nb_elem_left = std::accumulate(nx_locals.begin()+rank+1,
				     nx_locals.end(),
				     std::size_t(0));
    Domain local_dom(dom_);
    local_dom[0] = resize(local_dom[0], {-idx_first_elem, -nb_elem_left});
    
    GRF<T, DIM, Function> field(comm_, std::move(raw_field), local_dom,
                                dom_, correlation_function_);

    if (handle_alloc)
        deallocate();
    return field;
}

template <typename T, size_t DIM, typename Function>
GRF<T, DIM, Function> GRFgenerator<T, DIM, Function>::generate()
{
    int rank         = comm_.rank();
    int size         = comm_.size();
    size_t nx_global = nb_samples(dom_)[0];

    size_t nx_local  = balanced_sharing(rank, size, nx_global);

    return generate(nx_local);
}

/**
 * @todo Sortir de GRFgenerator
 *
 * @brief Extract the real part of the required part of data_.
 * */
template <typename T, size_t DIM, typename Function>
std::vector<T> GRFgenerator<T, DIM, Function>::build_champ(
    const size_t ny_input, const size_t nz_input, const size_t nijk_i,
    const size_t Ny_fft, const size_t Nz_fft, const size_t local_nx,
    const size_t local_x_start,
    const std::vector<std::complex<T>, fftw::fftw_allocator<std::complex<T>>>
        &data)
{
    int tag_nbr = 2, tag_tab = 4;
    int pp  = comm_.size(); // size of communicator
    int num = comm_.rank(); // ranki of current proc

    std::vector<T> champ(nijk_i * ny_input * nz_input);

    size_t borne_1 = local_x_start;
    size_t borne_2 = local_x_start + local_nx;

    std::vector<MPI_Request> sendRequest;
    std::vector<MPI_Request> recvRequest;

    // step 1. search receiving processors [num_first_proc_dest;
    // num_last_proc_dest] to which the current process will send the data
    // if any (num_first_proc_dest=num_last_proc_dest=-1 otherwise)
    auto list_local_nx      = comm_.all_gather(local_nx);

    auto list_nijk_i        = comm_.all_gather(nijk_i);
    int num_first_proc_dest = -1;
    int num_last_proc_dest  = -1;
    size_t somme_nijk_i     = 0;

    size_t ij;
    bool loop = true;
    for (int i = 0; i < pp && loop; i++)
    { // liste des processeurs destinataires

        if (somme_nijk_i <= borne_1)
        {
            num_first_proc_dest = i;
        }
        somme_nijk_i += list_nijk_i.at(i);
        if (somme_nijk_i >= borne_2)
        {
            num_last_proc_dest = i;
            loop               = false;
        }
        if (i == pp - 1)
        {
            num_last_proc_dest = i;
            loop               = false;
        }
    }

    if (somme_nijk_i <= borne_1)
    {
        num_first_proc_dest = -1;
        num_last_proc_dest  = -1;
    }

    size_t nbProcDest;
    if (num_first_proc_dest == -1)
        nbProcDest = 0;
    else
        nbProcDest = num_last_proc_dest - num_first_proc_dest +
                     1; // nombre de processeurs auxquels le processeur
                        // courant doit adresser des donnees
    // cout << "nbProcDest" << nbProcDest << "num_first_proc_dest " <<
    // num_first_proc_dest << "num_last_proc_dest" << num_last_proc_dest <<
    // endl;
    if (local_nx == 0)
    { // Cas ou il n'y a rien a envoyer
        nbProcDest          = 0;
        num_first_proc_dest = -1;
        num_last_proc_dest  = -1;
    }
    // std::cout << "Proc" << num << "nbProcDest :" << nbProcDest <<
    // std::endl;

    std::vector<size_t> partDest;
    std::vector<size_t> nbrCol;
    somme_nijk_i = 0;
    for (int i = 0; i < num_first_proc_dest; i++)
    {
        somme_nijk_i += list_nijk_i.at(i);
    }

    size_t somme_tot_nijk_i = 0;
    for (int i = 0; i < pp; i++)
    {
        somme_tot_nijk_i += list_nijk_i.at(i);
    }

    size_t somNx = 0;
    for (int i = 0; i < num; i++)
        somNx += list_local_nx.at(i);

    size_t somDest         = somme_nijk_i - somNx;

    bool ProcEmMe          = false;
    size_t nbProcEmNotMe   = 0;
    bool ProcDestMe        = false;
    size_t nbProcDestNotMe = 0;

    if ((num >= num_first_proc_dest) && (num <= num_last_proc_dest))
    {
        ProcDestMe      = true;
        nbProcDestNotMe = nbProcDest - 1;
    }
    else
        nbProcDestNotMe = nbProcDest;

    // step 2. search the number of columns to send to each receiving
    // processor (if any): vector PartDest
    size_t somEmprev;
    size_t somEmnew;
    somEmnew = std::min(somme_tot_nijk_i - local_x_start, local_nx);
    for (size_t i = 0; i < nbProcDest; i++)
    {
        nbrCol.resize(nbProcDest);
        if ((int)i + num_first_proc_dest < 0)
            somEmprev = somEmnew;
        else
        {
            somEmprev = std::min(
                somEmnew, (size_t)list_nijk_i.at(i + num_first_proc_dest));
            somDest += list_nijk_i.at(i + num_first_proc_dest);
        }
        somEmnew =
            std::min(somme_tot_nijk_i - local_x_start, local_nx) - somDest;
        nbrCol.at(i) = std::min(somDest, somEmprev);
        // std::cout << "Proc" << num << "nbrCol.at(i)" << nbrCol.at(i) <<
        // std::endl;
    }

    if (nbProcDest > 0)
    {
        partDest.resize(nbProcDest);

        if (local_nx != 0)
        {
            partDest.at(0) = nbrCol.at(0);
            // std::cout << "Proc" << num << ": partDest.at(0)" <<
            // partDest[0]
            // << std::endl;
        }

        for (size_t i = 1; i < nbProcDest; i++)
        {
            partDest.at(i) = partDest.at((size_t)i - 1) + nbrCol.at(i);
            // std::cout << "Proc" << num << "partDest.at(i)." <<
            // partDest.at(i)
            // << std::endl;
        }
    }

    // step 3. search sending processors [num_first_proc_em;
    // num_last_proc_em] that will send the data to the current processor if
    // any (num_first_proc_em=num_last_proc_em=-1 otherwise)
    int num_first_proc_em     = -1;
    int num_last_proc_em      = -1;
    size_t local_nijk_i_start = 0;
    for (int i = 0; i < num; i++)
    {
        local_nijk_i_start +=
            list_nijk_i.at(i); // TODO le mettre plus haut et le diffuser
    }

    size_t local_nijk_i_end = local_nijk_i_start + nijk_i;
    // std::cout << "Proc" << num << "local_nijk_i_start : " <<
    // local_nijk_i_start << std::endl; std::cout << "Proc" << num <<
    // "local_nijk_i_end : " << local_nijk_i_end << std::endl;
    size_t somme_local_nx   = 0;
    loop                    = true;
    for (int i = 0; i < pp && loop; i++)
    { // liste des processeurs emetteurs
        if (somme_local_nx <= local_nijk_i_start)
        {
            num_first_proc_em = i;
        }
        somme_local_nx += list_local_nx.at(i);
        if (somme_local_nx >= local_nijk_i_end)
        {
            num_last_proc_em = i;
            loop             = false;
        }
    }

    size_t nbProcEm = num_last_proc_em - num_first_proc_em + 1;
    // std::cout << "Proc" << num << "nbProcEm : " << nbProcEm << std::endl;

    if ((num >= num_first_proc_em) && (num <= num_last_proc_em))
    {
        ProcEmMe      = true;
        nbProcEmNotMe = nbProcEm - 1;
        // ind_proc_local = num - num_first_proc_em;
    }
    else
        nbProcEmNotMe = nbProcEm;

    std::vector<size_t> list_nbcol_recv;
    std::vector<size_t> ProcEmNotMe;
    //	std::vector<int> ind_proc_local_Em;
    std::vector<std::vector<T>> data_recv;
    if (nbProcEmNotMe > 0)
    {
        //	ind_proc_local_Em.resize(nbProcEmNotMe);
        ProcEmNotMe.resize(nbProcEmNotMe);
        list_nbcol_recv.resize(nbProcEmNotMe);
        data_recv.resize(nbProcEmNotMe);
    }
    // count the number of proc below me that send me data (if any)
    size_t count = 0;
    for (size_t i = 0; i < nbProcEmNotMe; i++)
    {
        if (num_first_proc_em + i < size_t(num))
            count++;
    }
    // std::cout << "Proc" << num << ": receive data from " << count
    //           << "proc below himself" << std::endl;

    // std::cout << "Proc" << num << ": nbProcDest" << nbProcDest << std::endl;
    // std::cout << "Proc" << num << ": nbProcEm" << nbProcEm << std::endl;
    // std::cout << "Proc" << num << ": nbProcDestNotMe" << nbProcDestNotMe
    //           << std::endl;
    // std::cout << "Proc" << num
    //           << ":"
    //              "nbProcEmNotMe "
    //           << nbProcEmNotMe << std::endl;
    // std::cout << " Proc " << num << ": num_first_proc_em" <<
    // num_first_proc_em
    //           << std::endl;
    // std::cout << "Proc" << num << ": num_last_proc_em" << num_last_proc_em
    //           << std::endl;

    std::vector<int> ProcDestNotMe(nbProcDestNotMe);
    // std::vector<int> ind_proc_local_Dest(nbProcDestNotMe);

    std::vector<size_t> list_nbcol_send(nbProcDest);
    std::vector<std::vector<T>> data_send(nbProcDestNotMe);
    std::vector<size_t> list_first_col(nbProcDest);
    std::vector<size_t> list_last_col(nbProcDest);

    if (!ProcEmMe)
    {
        size_t i = 0;
        for (int n = num_first_proc_em; n <= num_last_proc_em; n++)
        {
            ProcEmNotMe[i] = n;
            i++;
        }
    }
    else
    {
        size_t i = 0;
        for (int n = num_first_proc_em; n <= num_last_proc_em; n++)
        {
            if (n != num)
            {
                ProcEmNotMe[i] = n;
                i++;
            }
        }
    }

    if (nbProcDestNotMe > 0)
    {
        size_t i = 0;
        for (int n = num_first_proc_dest; n <= num_last_proc_dest; n++)
        {
            if (n != num)
            {
                ProcDestNotMe[i] = n;
                i++;
            }
        }
    }

    // step 4. receive the number of column that will be send by the sending
    // processors [num_first_proc_em; num_last_proc_em] to the current
    // processor
    for (size_t i = 0; i < nbProcEmNotMe; i++)
    {
        recvRequest.resize(recvRequest.size() + 1);
        MPI_Irecv(&list_nbcol_recv[i], sizeof(size_t), MPI_BYTE, ProcEmNotMe[i],
                  tag_nbr, comm_.comm(), &recvRequest[recvRequest.size() - 1]);
    }

    // step 5. send the number of columns to the receiving processors
    // ProcDestNotMe
    if (nbProcDest > 0)
    {
        list_first_col[0]  = 0;
        list_last_col[0]   = partDest[0];
        list_nbcol_send[0] = list_last_col[0] - list_first_col[0];
        // std::cout << "Proc" << num << " envoie :nb col courante" <<
        // list_nbcol_send[0] << std::endl;
    }
    if ((nbProcDest >= 1) &&
        (!ProcDestMe)) // the current proc is not receiving data from
                       // himself (only send to others)
    {
        sendRequest.resize(sendRequest.size() + 1);
        MPI_Isend(&list_nbcol_send[0], sizeof(size_t), MPI_BYTE,
                  ProcDestNotMe[0], tag_nbr, comm_.comm(),
                  &sendRequest[sendRequest.size() - 1]);
        // std::cout << "Proc" << num << ":Envoi de : " <<
        // list_nbcol_send[0] << " colonnes au proc  =" << ProcDestNotMe[0]
        // << std::endl;
        for (size_t i = 1; i < nbProcDest; i++)
        {
            list_first_col[i] = partDest[i - 1];
            list_last_col[i]  = partDest[i];
            if (local_nx < partDest[i - 1])
                list_last_col[i] = local_nx;
            list_nbcol_send[i] = list_last_col[i] - list_first_col[i];
            sendRequest.resize(sendRequest.size() + 1);
            MPI_Isend(&list_nbcol_send[i], sizeof(size_t), MPI_BYTE,
                      ProcDestNotMe[i], tag_nbr, comm_.comm(),
                      &sendRequest[sendRequest.size() - 1]);
        }
    }
    else
    { // the current proc is receiving data from himself and send data to
      // others
      // -> // sent is done from list_first_col[i>0], list_first_col[0] is
      // dedicated to the number of columns he will get from himself
        for (size_t i = 1; i < nbProcDest; i++)
        {
            list_first_col[i] = partDest[i - 1];
            list_last_col[i]  = partDest[i];
            if (local_nx < partDest[i - 1])
                list_last_col[i] = local_nx;
            list_nbcol_send[i] = list_last_col[i] - list_first_col[i];
            sendRequest.resize(sendRequest.size() + 1);
            MPI_Isend(&list_nbcol_send[i], sizeof(size_t), MPI_BYTE,
                      ProcDestNotMe[i - 1], tag_nbr, comm_.comm(),
                      &sendRequest[sendRequest.size() - 1]);
            // std::cout << "Proc" << num << ":Envoi nbr colonnes." <<
            // list_nbcol_send[i] << " envoyées au proc  =" <<
            // ProcDestNotMe[i - 1] << std::endl;
        }
    }

    if ((pp > 1) & (nijk_i > 0))
    {
        if (recvRequest.size() > 0)
        {
            MPI_Waitall(recvRequest.size(), &recvRequest[0],
                        MPI_STATUSES_IGNORE);
            recvRequest.resize(0);
        }
    }

    // step 5. receive the data on the current proc
    size_t nbdata_recv;
    for (size_t i = 0; i < nbProcEmNotMe; i++)
    {
        nbdata_recv = list_nbcol_recv[i] * ny_input * nz_input;
        // std::cout << "Proc" << num << "nbdata_recv:" << nbdata_recv <<
        // std::endl;
        data_recv[i].resize(nbdata_recv);
        recvRequest.resize(recvRequest.size() + 1);
        MPI_Irecv(data_recv[i].data(), nbdata_recv * sizeof(T), MPI_BYTE,
                  ProcEmNotMe[i], tag_tab, comm_.comm(),
                  &recvRequest[recvRequest.size() - 1]);
    }

    // number of column received from below
    size_t shift = 0;
    for (size_t i = 0; i < count; i++)
    {
        shift += list_nbcol_recv[i];
    }
    // std::cout << "Proc" << num << "Reception données ok: shift" << shift
    // << std::endl; std::cout << "Proc" << num << "Reception données ok:
    // ProcDestMe" << ProcDestMe << std::endl;
    // // step 6. if the current proc already has a part of the field, cast
    // it to the type of champ
    if (ProcDestMe)
    {
        // int nbr = list_nbcol_send[num - num_first_proc_dest] * (ny_input
        // * nz_input); std::cout << "Proc" << num << ":nbr of column to
        // copy:" << nbr << std::endl; std::cout << "Proc" << num <<
        // ":list_first_col[num
        // - num_first_proc_dest]:" << list_first_col[num -
        // num_first_proc_dest]
        // + shift << std::endl; std::cout << "Proc" << num <<
        // ":list_last_col[num - num_first_proc_dest]:" << list_last_col[num
        // - num_first_proc_dest] + shift << std::endl;
        size_t ijd;
        size_t ijc;
        for (size_t i = list_first_col[num - num_first_proc_dest] + shift;
             i < list_last_col[num - num_first_proc_dest] + shift; i++)
        {
            for (size_t j = 0; j < ny_input; j++)
            {
                for (size_t k = 0; k < nz_input; k++)
                {
                    ijd = index_grid_to_vector(i - shift, j, k, Ny_fft, Nz_fft);
                    ijc = index_grid_to_vector(i, j, k, ny_input, nz_input);
                    // std::cout << "Proc" << num << ":ijd:" << ijd <<
                    // ":data[ijd].real():" << data[ijd].real() <<
                    // std::endl; std::cout << "Proc" << num << ":ijc:" <<
                    // ijc <<
                    // ":champ[ijc]:" << champ[ijc] << std::endl;
                    champ[ijc] = data[ijd].real();
                }
            }
        }
        // std::cout << "Proc" << num << ": copy of data in field ok." <<
        // std::endl;
    }

    // step 7. Send the data to the receiving processor
    if ((nbProcDest >= 1) &&
        (!ProcDestMe)) // the current proc is not receiving data from
                       // himself (only send to others)
    {
        for (size_t ii = 0; ii < nbProcDestNotMe; ii++)
        {
            // std::cout << "Proc" << num << "list_nbcol_send:" <<
            // list_nbcol_send[ii] << std::endl;
            size_t nbr = list_nbcol_send[ii] * (ny_input * nz_input);
            // std::cout << "Proc" << num << "nbr data send:" << nbr <<
            // std::endl; std::cout << "Proc" << num <<
            // "list_first_col[ii+1]:"
            // << list_first_col[ii] << "list_last_col[ii+1]" <<
            // list_last_col[ii] << std::endl;

            data_send[ii].resize(nbr);
            size_t comp = 0;

            for (size_t i = list_first_col[ii]; i < list_last_col[ii]; i++)
            {
                for (size_t j = 0; j < ny_input; j++)
                {
                    for (size_t k = 0; k < nz_input; k++)
                    {
                        ij = index_grid_to_vector(i, j, k, Ny_fft, Nz_fft);
                        data_send[ii][comp] = data[ij].real();
                        comp++;
                    }
                }
            }
            // std::cout << "Proc" << num << "-------------------before sent
            // data -----------------" << comp << " à proc " <<
            // ProcDestNotMe[ii] << std::endl;
            sendRequest.resize(sendRequest.size() + 1);
            MPI_Isend(data_send[ii].data(), comp * sizeof(T), MPI_BYTE,
                      ProcDestNotMe[ii], tag_tab, comm_.comm(),
                      &sendRequest[sendRequest.size() - 1]);
        }
    }
    else
    { // the current proc is receiving data from himself and send data to
      // others
      // -> // sent is done from list_first_col[i>0], list_first_col[0] is
      // dedicated to the number of columns he will get from himself
        for (size_t ii = 0; ii < nbProcDestNotMe; ii++)
        {
            // std::cout << "Proc" << num << "list_nbcol_send:" <<
            // list_nbcol_send[ii + 1] << std::endl;
            size_t nbr = list_nbcol_send[ii + 1] * (ny_input * nz_input);
            // std::cout << "Proc" << num << "nbr data send:" << nbr <<
            // std::endl;
            // std::cout << "Proc" << num << "list_first_col[ii+1]:" <<
            // list_first_col[ii + 1] << "list_last_col[ii+1]" <<
            // list_last_col[ii + 1] << std::endl;

            data_send[ii].resize(nbr);
            size_t comp = 0;

            for (size_t i = list_first_col[ii + 1]; i < list_last_col[ii + 1];
                 i++)
            {
                for (size_t j = 0; j < ny_input; j++)
                {
                    for (size_t k = 0; k < nz_input; k++)
                    {
                        ij = index_grid_to_vector(i, j, k, Ny_fft, Nz_fft);
                        data_send[ii][comp] = data[ij].real();
                        comp++;
                    }
                }
            }
            sendRequest.resize(sendRequest.size() + 1);
            MPI_Isend(data_send[ii].data(), comp * sizeof(T), MPI_BYTE,
                      ProcDestNotMe[ii], tag_tab, comm_.comm(),
                      &sendRequest[sendRequest.size() - 1]);
        }
    }
    // std::cout << "Proc" << num << ":All - Reception données ok" <<
    // std::endl;
    if ((pp > 1) & (nijk_i > 0))
    {
        if (recvRequest.size() > 0)
        {
            MPI_Waitall(recvRequest.size(), &recvRequest[0],
                        MPI_STATUSES_IGNORE);
            recvRequest.resize(0);
        }
    }

    MPI_Barrier(comm_.comm());
    if ((pp > 1) & (nijk_i > 0))
    {
        if (sendRequest.size() > 0)
        {
            MPI_Waitall(sendRequest.size(), &sendRequest[0],
                        MPI_STATUSES_IGNORE);
            sendRequest.resize(0);
        }
    }

    // step 8. Copy the data
    size_t pos_column = 0;
    for (size_t ii = 0; ii < nbProcEmNotMe; ii++)
    {
        size_t comp = 0;
        for (size_t i = pos_column; i < pos_column + list_nbcol_recv[ii]; i++)
        {
            for (size_t j = 0; j < ny_input; j++)
            {
                for (size_t k = 0; k < nz_input; k++)
                {
                    ij = index_grid_to_vector(i, j, k, ny_input, nz_input);
                    champ[ij] = data_recv[ii][comp];
                    comp++;
                }
            }
        }
        pos_column += list_nbcol_recv[ii];
    }
    return champ;
}
} // namespace paracirce

#endif // __GRF_H_
