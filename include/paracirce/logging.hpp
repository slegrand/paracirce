/**
 * This file is part of the Paracirce software.
 *
 * For academic usage, you can use this software under the GNU Affero General
 * Public version 3 as published by the Free Software Foundation. Read the
 * LICENSE file in the root directory of this source tree for more information.
 *
 * For commercial usage, please contact the project members to purchase this
 * software under proprietary license,
 *
 * Copyright (C) 2021, Inria
 *
 * @file logging.hpp
 *
 * @brief Singleton Logger class. Used mainly for debugging purposes.
 *
 * @note Warning: Add json dependency, cannot be used as is.
 *
 * @author Geraldine PICHOT and Simon LEGRAND
 * */
#ifndef __LOGGING_H_
#define __LOGGING_H_

#include <fstream>
#include <nlohmann/json.hpp>
#include <string>

using json = nlohmann::json;

class Logger
{
    static Logger Instance_;
    json o_;
    Logger()                          = default;
    Logger(const Logger &)            = delete;
    Logger &operator=(const Logger &) = delete;

  public:
    static Logger &Instance() noexcept { return Instance_; };
    template <typename T> void operator()(std::string s, T val)
    {
        o_[s] = val;
    };
    void write(std::string log_file);
};

Logger Logger::Instance_;

void Logger::write(std::string log_file)
{
    std::ofstream of;
    of.open(log_file, std::ios::trunc);
    if (of.is_open())
        of << o_;

    of.close();
}
#endif // __LOGGING_H_
