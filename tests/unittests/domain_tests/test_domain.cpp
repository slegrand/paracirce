#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN
#include "doctest/doctest.h"
#include "paracirce/domain.hpp"
#include <utility>
#include <vector>
#include <iostream>

using namespace paracirce;

TEST_CASE("Range manipulation")
{
    auto r = Range<double>{0., 1., 101};

    /** resizement */
    auto enl_r = resize(r, {4,4}); // Add 4 samples on each side
    std::cout << enl_r << std::endl;
    REQUIRE(enl_r == Range<double>{-0.04, 1.04, 109});

    auto shr_r = resize(enl_r, {-4,-4}); // Remove 4 samples on each side
    REQUIRE(r == shr_r);

    // auto empty_r = Range<double>{0., 0., 0};

}

TEST_CASE("Domain instanciation")
{
    using namespace paracirce;

    /** Test initializer list */
    Range<double> a_r{0., 1., 10};
    Range<double> b_r(0., 1., 10);
    REQUIRE(a_r == b_r);

    DiscretDomain<double, 2> dom{{ {0., 1., 100},
                                   {0., 1., 100} }};
    REQUIRE(nb_samples(dom)[0] == 100);


    auto x_range = Range<double>{0., 1., 11}; // [0., 1.] with 11 samples
    auto y_range = Range<double>{0., 2., 11}; // [0., 2.] with 11 samples
    auto D_2D = DiscretDomain<double, 2>{x_range, y_range};

    REQUIRE(lengths(D_2D) == std::array<double,2>{1., 2.});
    REQUIRE(steps(D_2D) == std::array<double,2>{0.1, 0.2});

    /** Copy operation */
    auto z_range(x_range);
    REQUIRE(x_range == z_range);

    /** Move operation */
    auto w_range(Range<double>(0., 1., 10));
    REQUIRE((w_range.l_bound() == 0. &&
             w_range.r_bound() == 1. &&
             w_range.n() == 10));
    // TODO Test failure of domain with negative length

    /** Range iterators */
    auto r = std::vector<double>(a_r.begin(), a_r.end());
}

TEST_CASE("Domain resizement")
{
    using namespace paracirce;

    DiscretDomain<double, 2> dom{{{0., 1., 101}, {0., 2., 201}}};
    auto other_dom = resize(dom, {{{0, 6}, {-3, 0}}});
    REQUIRE(other_dom == DiscretDomain<double, 2>{{{0., 1.06, 107},{0.03, 2., 198}}});
}

TEST_CASE("SubDomain creation")
{
    using namespace paracirce;
    DiscretDomain<double, 2> D_2D{{{1.5, 3.5, 11}, {0., 2., 21}}};
    std::array<std::pair<size_t, size_t>, 2> subDomIdx{std::make_pair(2, 5),
                                                       std::make_pair(0, 20)};
    auto subDom = create_subdomain(D_2D, subDomIdx);

    std::array<std::pair<double, double>, 2> expected_bounds{
        std::make_pair(1.9, 2.5), std::make_pair(0., 2.)};
    REQUIRE(bounds(subDom) == expected_bounds);
}

TEST_CASE("Symmetric sampling")
{
    using namespace paracirce;
    double d = 1.0;

    size_t n_even = 10;
    auto res_sym_samp_even =
        std::vector<double>{0., 1., 2., 3., 4., 5., 4., 3., 2., 1.};
    REQUIRE(symmetric_sampling_1d(d, n_even, 0, n_even) == res_sym_samp_even);

    size_t n_odd = 9;
    auto res_sym_samp_odd =
        std::vector<double>{0., 1., 2., 3., 4., 4., 3., 2., 1.};
    REQUIRE(symmetric_sampling_1d(d, n_odd, 0, n_odd) == res_sym_samp_odd);

    size_t n          = 1;
    auto res_sym_samp = std::vector<double>{0.};
    REQUIRE(symmetric_sampling_1d(d, n, 0, n) == res_sym_samp);
}
