#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN
#include "doctest/doctest.h"
#include "paracirce/domain.hpp"
#include "paracirce/fft_api.hpp"
#include "paracirce/functions.hpp"
#ifdef PARACIRCE_HAS_OMP
#include <omp.h>
#endif
TEST_CASE("Function creation pattern")
{
    using namespace paracirce;
    auto correl_func_2D =
        create_function<double, 2>("gaussian", std::array{1., 1.});
    auto other_correl_func_2D = Gaussian<double, 2>({1., 1.});
    REQUIRE(std::get<3>(correl_func_2D)({2., 2.}) ==
            other_correl_func_2D({2., 2.}));

    auto correl_func_3D =
        create_function<double, 3>("matern", std::array{1., 1., 1.}, 4.);
    auto other_correl_func_3D = Matern<double, 3>({1., 1., 1.}, 4.);
    REQUIRE(std::get<4>(correl_func_3D)({6., 5., 4.}) ==
            other_correl_func_3D({6., 5., 4.}));
}
#ifdef PARACIRCE_HAS_OMP
TEST_CASE("omp_function_sampling")
{
    using namespace paracirce;

    double d = 1.0, n_even = 1022;
    auto x = symmetric_sampling_1d(d, n_even, 0, n_even);
    auto y = symmetric_sampling_1d(d, n_even, 0, n_even);

    auto f = Gaussian<double, 2>({1.0, 1.0});

    std::vector<std::complex<double>,
                fftw::fftw_allocator<std::complex<double>>>
        data1;
    std::vector<std::complex<double>,
                fftw::fftw_allocator<std::complex<double>>>
        data2;
    data1.resize(n_even * n_even);
    data2.resize(n_even * n_even);

    omp_set_num_threads(8);
    sampling_complex_2D(f, x, y, data1);

    omp_set_num_threads(1);
    sampling_complex_2D(f, x, y, data2);

    REQUIRE(data1 == data2);
}
#endif
