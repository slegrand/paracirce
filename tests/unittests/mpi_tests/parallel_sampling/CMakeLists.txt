set(CMAKE_CXX_STANDARD 17)

set(TEST_NAME "mpi_functions_sampling")

add_executable(${TEST_NAME})

set_target_properties(${TEST_NAME} PROPERTIES CXX_STANDARD 17 CXX_STANDARD_REQUIRED ON)

target_sources(
  ${TEST_NAME}
  PRIVATE
  main_mpi.cpp
  test_mpi_functions_sampling.cpp
)

include(tests_warnings)

target_link_libraries(
  ${TEST_NAME}
  PRIVATE
  doctest::doctest
  Paracirce::Paracirce
)

add_test(NAME ${TEST_NAME} COMMAND mpiexec --oversubscribe -n 3 ${TEST_NAME})
