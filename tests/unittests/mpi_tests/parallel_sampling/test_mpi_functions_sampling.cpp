#include "doctest/extensions/doctest_mpi.h"
#include "mpi.h"
#include "paracirce.hpp"

SCENARIO("Parallel symmetric sampling")
{
    int rank = -1;
    int size = -1;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    size_t lcl_x_strt = 0;
    size_t lcl_nx     = 0;
    std::vector<double> lcl_res;
    using namespace paracirce;

    GIVEN("Step is one")
    {
        double steps = 1.0;

        WHEN("Number of samples is even, n = 10")
        {
            /**
             * The global sampled array must be {0.,1.,2.,3.,4.,5.,4.,3.,2.,1.}
             */

            size_t n_even = 10;
            if (rank == 0)
            {
                lcl_x_strt = 0;
                lcl_nx     = 3;
                lcl_res.insert(lcl_res.end(), {0., 1., 2.});
            }
            if (rank == 1)
            {
                lcl_x_strt = 3;
                lcl_nx     = 3;
                lcl_res.insert(lcl_res.end(), {3., 4., 5.});
            }
            if (rank == 2)
            {
                lcl_x_strt = 6;
                lcl_nx     = 4;
                lcl_res.insert(lcl_res.end(), {4., 3., 2., 1.});
            }
            auto res = symmetric_sampling_1d(steps, lcl_nx, lcl_x_strt, n_even);

            THEN("Samples must be spread this way")
            {
                if (rank == 0)
                {
                    REQUIRE(res == lcl_res);
                }
                if (rank == 1)
                {
                    REQUIRE(res == lcl_res);
                }
                if (rank == 2)
                {
                    REQUIRE(res == lcl_res);
                }
            }
        }

        WHEN("Number of samples is odd, n = 9")
        {
            /**
             * The global sampled array must be {0.,1.,2.,3.,4.,4.,3.,2.,1.}
             */

            size_t n_odd = 9;
            if (rank == 0)
            {
                lcl_x_strt = 0;
                lcl_nx     = 3;
                lcl_res.insert(lcl_res.end(), {0., 1., 2.});
            }
            if (rank == 1)
            {
                lcl_x_strt = 3;
                lcl_nx     = 3;
                lcl_res.insert(lcl_res.end(), {3., 4., 4.});
            }
            if (rank == 2)
            {
                lcl_x_strt = 6;
                lcl_nx     = 3;
                lcl_res.insert(lcl_res.end(), {3., 2., 1.});
            }
            auto res = symmetric_sampling_1d(steps, lcl_nx, lcl_x_strt, n_odd);

            THEN("Samples must be spread this way")
            {
                if (rank == 0)
                {
                    REQUIRE(res == lcl_res);
                }
                if (rank == 1)
                {
                    REQUIRE(res == lcl_res);
                }
                if (rank == 2)
                {
                    REQUIRE(res == lcl_res);
                }
            }
        }
    }
}

TEST_CASE("GRF planners effort")
{
    using namespace paracirce;

    DiscretDomain<double, 2> dom{{{0., 31., 32}, {0., 31., 32}}};
    auto f_correl  = Gaussian<double, 2>({1., 1.});
    auto generator = GRFgenerator(MPI_COMM_WORLD, dom, f_correl);

    // Default value check
    REQUIRE(generator.planners_effort() == std::string("FFTW_ESTIMATE"));

    generator.planners_effort("FFTW_PATIENT");
    REQUIRE(generator.planners_effort() == std::string("FFTW_PATIENT"));

    generator.allocate();
    REQUIRE(generator.is_planned() == true);

    // Allocation invalidation
    generator.planners_effort("FFTW_MEASURE");
    REQUIRE(generator.is_planned() == false);

    CHECK_THROWS_AS(generator.planners_effort("INVALID_EFFORT"),
                    UnknownPlannersEffort);
}
