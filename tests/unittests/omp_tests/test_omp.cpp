#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN
#include "doctest/doctest.h"
#include "paracirce.hpp"
#include <iostream>
#include <omp.h>
#include <vector>

using namespace paracirce;

TEST_CASE("phase_omp")
{
    auto N = 8;
    std::vector<std::complex<double>,
                fftw::fftw_allocator<std::complex<double>>>
        res_1_th(N, 1.);
    std::vector<std::complex<double>,
                fftw::fftw_allocator<std::complex<double>>>
        res_2_th(N, 1.);
    std::vector<std::complex<double>,
                fftw::fftw_allocator<std::complex<double>>>
        res_3_th(N, 1.);
    std::vector<std::complex<double>,
                fftw::fftw_allocator<std::complex<double>>>
        res_4_th(N, 1.);
    std::vector<std::complex<double>,
                fftw::fftw_allocator<std::complex<double>>>
        res_10_th(N, 1.);
    Rng r{"test_omp"};

    /**
     * No need to reset the rng before each call, since local
     * copies are used in compute_phases.
     * */

    omp_set_num_threads(1);
    compute_phases(res_1_th, r, 1.);
    for (auto const &e : res_1_th)
        std::cout << e << "\n";

    omp_set_num_threads(2);
    compute_phases(res_2_th, r, 1.);
    for (auto const &e : res_2_th)
        std::cout << e << "\n";

    omp_set_num_threads(3);
    compute_phases(res_3_th, r, 1.);
    for (auto const &e : res_3_th)
        std::cout << e << "\n";

    omp_set_num_threads(4);
    compute_phases(res_4_th, r, 1.);
    for (auto const &e : res_4_th)
        std::cout << e << "\n";

    omp_set_num_threads(10);
    compute_phases(res_10_th, r, 1.);
    for (auto const &e : res_10_th)
        std::cout << e << "\n";

    bool res = true;
    for (int i = 0; i < N; i++)
    {
        if ((res_1_th[i] != res_2_th[i]) || (res_1_th[i] != res_3_th[i]) ||
            (res_1_th[i] != res_4_th[i]) || (res_1_th[i] != res_10_th[i]))
            res = false;
    }
    REQUIRE(res);
}
